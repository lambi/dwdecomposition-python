The modules were written in Python 2.7.

The solvers (solve.py and lpsolver.py) need cplex and the fast downward translator installed to work. 
The solver solve.py also uses the module abstractions.py,  it has to be in the same directory as the translator.
