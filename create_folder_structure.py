#!/usr/bin/env python

import csv
import os
import re
import shutil
from datetime import datetime
import argparse
from random import shuffle
from properties import Properties

mem_max = 3872000000
TIME_LIMIT=6000
initials = "vh"
dir_path = os.path.dirname(os.path.realpath(__file__))
downward_benchmarks_path = os.environ["DOWNWARD_BENCHMARKS"]

parser = argparse.ArgumentParser(description='Create a folder structure for slurm')
parser.add_argument("--partition", type=str, default="infai_1")
parser.add_argument("--mem-per-cpu", type=int, default=mem_max)
parser.add_argument("--lpsolver", default=False, action="store_true")

args = parser.parse_args()

run_file = "solve.py"
if (args.lpsolver):
    run_file = "lpsolver.py"

if (args.mem_per_cpu > mem_max):
    raise ValueError("mem_per_cpu too high")

def generate_job_name():
    return initials + "-" + datetime.now().strftime("%m-%d-%Y-%H:%M:%S")

experiment_name = generate_job_name()
base_path = os.path.join(dir_path, "experiments", experiment_name)

def copy_files(run_dir, domain, problem):
    domain_file_name = 'domain.pddl'

    shutil.copyfile(os.path.join(dir_path, downward_benchmarks_path, row[0] + "/" + row[1]), os.path.join(run_folder, 'problem.pddl'))
    problem_label = row[1][0:3]
    try:
        shutil.copyfile(os.path.join(dir_path, downward_benchmarks_path, row[0] + "/domain.pddl"), os.path.join(run_folder, 'domain.pddl'))
    except:
        try:
            shutil.copyfile(os.path.join(dir_path, downward_benchmarks_path, row[0] + "/" + problem_label + "-domain.pddl"), os.path.join(run_folder, 'domain.pddl'))
        except:
            try:
                shutil.copyfile(os.path.join(dir_path, downward_benchmarks_path, row[0] + "/domain_" + problem_label + ".pddl"), os.path.join(run_folder, 'domain.pddl'))
            except:
                try:
                    problem_label = row[1][0:5]
                    shutil.copyfile(os.path.join(dir_path, downward_benchmarks_path, row[0] + "/domain_" + problem_label + ".pddl"), os.path.join(run_folder, 'domain.pddl'))
                except:
                    print "Did not find domain file for problem " + row[0] + " " + row[1] + ", tried " + problem_label


i = 0

with open("initial_h_values.csv", "rb") as valuesFile:
    reader = csv.reader(valuesFile, delimiter="\t")
    for row in reader:
        i = i + 1

        run_folder = os.path.join(base_path, "run_" + `i`)
        os.makedirs(run_folder)

        copy_files(run_folder, row[0], row[1])

        props = Properties(os.path.join(run_folder, 'info'))
        props["domain"] = row[0]
        props["problem"] = row[1]
        props.write()

    shutil.copy(os.path.join(dir_path, run_file), os.path.join(base_path, 'solve.py'))
    shutil.copy(os.path.join(dir_path, "parser.py"), os.path.join(base_path, 'parser.py'))
    shutil.copy(os.path.join(dir_path, "properties.py"), os.path.join(base_path, 'properties.py'))
    shutil.copytree(os.path.join(dir_path, "fast-downward-src"), os.path.join(base_path, "fast-downward-src"))

    file_handle = open("run.slurm.template", "r")
    slurm_script = file_handle.read()
    file_handle.close()

    slurm_script = slurm_script.replace("$$jobName", experiment_name)
    slurm_script = slurm_script.replace("$$partition", args.partition)
    slurm_script = slurm_script.replace("$$memoryLimit", `args.mem_per_cpu`.replace('000000', 'M'))
    slurm_script = slurm_script.replace("$$numberOfTasks", `i`)
    slurm_script = slurm_script.replace("$$timeLimit", str(TIME_LIMIT))


    ids = range(1, i + 1)
    shuffle(ids)
    slurm_script = slurm_script.replace("$$shuffledIds", " ".join(str(x) for x in ids))

    f = open(os.path.join(base_path, "run.slurm"), "w")
    f.write(slurm_script)
    f.close()
