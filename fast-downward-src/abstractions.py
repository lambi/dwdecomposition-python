#!/usr/bin/env python

from sas_tasks import SASOperator


class Abstraction:
    def __init__(self, num_abstract_states, initial_state, goal_state, transitions):
        # Number of abstract states in this abstraction (integer).
        # Abstract states are the numbers {0, 1, ..., num_abstract_states-1}
        self.num_abstract_states = num_abstract_states

        # ID of the abstract initial state (integer)
        self.initial_state = initial_state

        # ID of the abstract goal state (integer)
        # We use a transformation that ensures that there is a unique goal state.
        self.goal_state = goal_state

        # Transitions between abstract states (list of tuples <v, o, w>
        # where v and w are integers indicating the start and end of
        # the transition, and o is an integer indicating the ID of the
        # operator).
        self.transitions = transitions

        # Operators that are irrelevant to this abstraction technically
        # have a self-loop on every abstract state but it is easier to
        # treat the constraints of these self-loops as variable bounds.
        # We store this as a set of operator IDs that is relevant.
        # LP variables for local cost functions (C^\alpha_i) should be
        # unrestricted if the ID of the operator is in this set and
        # restricted to non-negative values otherwise.
        self.relevant_operators = set(op_id for _, op_id, _ in transitions)


def create_atomic_projections(task):
    return [create_atomic_projection(task, var) for var in range(len(task.variables.ranges))]

def create_atomic_projection(task, var):
    num_abstract_states = task.variables.ranges[var]
    unknown_value = num_abstract_states

    initial_state = task.init.values[var]
    requires_transformation = False
    for goal_var, goal_val in task.goal.pairs:
        if goal_var == var:
            goal_state = goal_val
            break
    else:
        goal_state = unknown_value
        requires_transformation = True

    transitions = []
    relevant_operators = set()

    for op_id, op in enumerate(task.operators):
        for op_var, pre, post, cond in op.pre_post:
            if op_var == var:
                if pre == -1:
                   requires_transformation = True
                   pre =  unknown_value
                transitions.append((pre, op_id, post))
        for op_var, val in op.prevail:
            if op_var == var:
                transitions.append((val, op_id, val))

    if requires_transformation:
        num_abstract_states += 1
        for val in range(num_abstract_states-1):
            op_id = len(task.operators)
            forget_op = SASOperator("(forget_var%d_val%d)" % (var, val),
                [], [(var, val, unknown_value, [])], 0)
            task.operators.append(forget_op)
            transitions.append((val, op_id, unknown_value))

    return Abstraction(num_abstract_states, initial_state, goal_state, transitions)


# if __name__ == "__main__":
#     import normalize
#     import options
#     import pddl_parser
#     from translate import pddl_to_sas
#
#     task = pddl_parser.open(
#             domain_filename=options.domain, task_filename=options.task)
#     normalize.normalize(task)
#     sas_task = pddl_to_sas(task)
#
#     abstractions = create_atomic_projections(sas_task)
#
#     for var, abstraction in enumerate(abstractions):
#         print "Projection to variable %d has %d abstract states" % (var, abstraction.num_abstract_states)
#         print "  init:", abstraction.initial_state
#         print "  goal:", abstraction.goal_state
#         print "  transitions"
#         for pre, op_id, post in abstraction.transitions:
#             print "    %d --%d--> %d %s cost: %d" % (pre, op_id, post, sas_task.operators[op_id].name, sas_task.operators[op_id].cost)
#         print
