try:
    import simplejson as json
except ImportError:
    import json

import os
import sys


def makedirs(path):
    """
    os.makedirs() variant that doesn't complain if the path already exists.
    """
    try:
        os.makedirs(path)
    except OSError:
        # Directory probably already exists.
        pass


def write_file(filename, content):
    with open(filename, 'w') as f:
        f.write(content)


class Properties(dict):
    def __init__(self, filename=None):
        self.filename = filename
        self.load(filename)
        dict.__init__(self)

    def __str__(self):
        return json.dumps(self, indent=2, separators=(',', ': '), sort_keys=True)

    def load(self, filename):
        if not filename or not os.path.exists(filename):
            return
        with open(filename) as f:
            try:
                self.update(json.load(f))
            except ValueError as e:
                print "JSON parse error in file '%s': %s" % (filename, e)
                sys.exit(1)


    def write(self):
        """Write the properties to disk."""
        assert self.filename
        makedirs(os.path.dirname(self.filename))
        write_file(self.filename, str(self))
