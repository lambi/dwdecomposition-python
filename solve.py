#!/usr/bin/env python

import sys
import os
dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(dir_path, "fast-downward-src"))
import pddl_parser
import normalize
import numpy as np
import cplex as cp
from translate import pddl_to_sas
from abstractions import create_atomic_projections
from numpy.linalg import inv
import time
import math

def optimizeLP(A, Rhs, Names, Objective, Senses, optSense, conNames, lower_bounds, upper_bounds):
    """Call the LP solver of Cplex, optimizing
    Args:
        A (array): array of arrays (originally array of row-vectors)
        Rhs (array): right hand side
        Names (array): column names
        Objective (array): objective function
        Senses (array): array containing relational signs for every row, can be one of equal(E), lessthanequal(L), greaterthanequal(G)
        optSense (string): "min" or "max" accordint to how we want to optimize
        conNames (array): constraint names
    Returns:
        if the LP has optimal solition: tuple("optimal", optimal solution)
        if the LP is unbounded: tuple("unbounded", extreme ray)
        if the LP is infeasible: string "infeasible"
    """
    problem = cp.Cplex()
    problem.set_log_stream(None)
    #problem.set_error_stream(None)
    problem.set_warning_stream(None)
    problem.set_results_stream(None)
    if optSense == "max":
        problem.objective.set_sense(problem.objective.sense.maximize)
    else:
        problem.objective.set_sense(problem.objective.sense.minimize)
    names = Names
    problem.variables.add(obj = Objective, lb = lower_bounds, ub = upper_bounds, names = Names)
    constraints = map(lambda row: [names, row], A)
    problem.linear_constraints.add(lin_expr = constraints,
                                   senses = Senses,
                                   rhs = Rhs)
    fake_vector=[0.0]*len(names)
    problem.parameters.preprocessing.presolve.set(problem.parameters.preprocessing.presolve.values.off)
    problem.solve()
    status = problem.solution.get_status_string()
    if status == "unbounded" :
        ray = problem.solution.advanced.get_ray()
    else: ray = "-1"
    value= problem.solution.get_values()
    if status == "optimal":
        return (status, value)
    elif status == "unbounded":
        return (status, ray)
    else: return (status, fake_vector)

def costpart_loop(num_of_operators, abstractionsLPs, num_task_operators, abstractions, operator_costs, num_of_abstractions, start_time):
    #Creating the master LP
    #The M matrix will contain num_of_operators independent vectors, which are the extreme points/rays of the subpolihedrons
    #Then changing the base
    #Finding the optimal solution
    abstraction_index=[]
    goals=[]
    shortest_path_values=[0.0]*num_of_operators
    sol_types=[]
    allresult=[]
    masterMatrix= np.identity(num_of_operators)  #rows of the master matrix
    if len(masterMatrix) > 40000:
        raise ValueError("Memory.")
    #M=map(list, zip(*masterMatrix))
    Mm= np.identity(num_of_operators)
    M= Mm.tolist()
    rightHS=operator_costs
    column_names=["Costpart"+`i` for i in range(len(shortest_path_values))]
    row_names = ["Index"+`i` for i in range(len(rightHS))]
    primal_LP_result = optimizeLP(masterMatrix, rightHS, column_names, shortest_path_values, ["L"]*len(rightHS), "max", row_names, [0]*len(shortest_path_values), [0]*len(shortest_path_values))
    y = np.linalg.solve(np.asarray(M).transpose(), shortest_path_values)
    x=np.asarray(primal_LP_result[1])
    primal_value= np.matmul(x, shortest_path_values)
    dual_value=np.matmul(y, rightHS)
    column_candidates=[]
    heur_values=[]
    reduced_cost=[]
    for i in range(0, num_of_abstractions):
        temp_output = get_and_transform_costpart_b(i, abstractionsLPs, num_task_operators, abstractions, y, num_of_operators)
        sol_types.append(temp_output[0])
        column_candidates.append(temp_output[1])
        H= shortest_path(i, abstractions, temp_output[3], num_of_operators, operator_costs)[1]
        #print shortest_path(i, abstractions, temp_output[3], num_of_operators, operator_costs)
        heur_values.append(H)
        reduced_cost.append(float(y.dot(temp_output[1])) - float(H))  #we created the reduced cost vector. now we proceed with the loop if it is necessary
    index_to_include_in_base=-1
    temp_reduced_cost = reduced_cost
    temp_base = M #this matrix will contain the actual set of columns of the master LP
    temp_column_candidates = column_candidates
    temp_shortest_path_values = shortest_path_values
    temp_abstraction_index = abstraction_index
    temp_heur_values = heur_values
    temp_mastermatrix = masterMatrix
    temp_x= x
    temp_primal_value = primal_value
    temp_dual_value = dual_value
    heuristic_estimation=[]
    heuristic_estimation.append(temp_primal_value)
    temp_goals = goals
    temp_y = y
    temp_column_names=column_names
    num_of_iterations=0
    temp_sol_types=sol_types
    all_sol_types=[]
    start_loop_time = time.time()
    while min(temp_reduced_cost) < 0:
        # if (time.time() - start_time) > 5000:
        #     raise ValueError("Timeout.")
        num_of_iterations = num_of_iterations +1
        index_to_include_in_base = temp_reduced_cost.index(min(temp_reduced_cost))
        all_sol_types.append(temp_sol_types[index_to_include_in_base])
        temp_abstraction_index.append(index_to_include_in_base)
        temp_goals.append(temp_heur_values[index_to_include_in_base])
        temp_shortest_path_values.append(temp_heur_values[index_to_include_in_base])
        temp_column_names.append("Costpart"+`len(temp_shortest_path_values)`)
        temp_base.append(temp_column_candidates[index_to_include_in_base])
        temp_mastermatrix = map(list, zip(*temp_base))
        temp_upperbound= [0]*num_of_operators + [cp.infinity]*int((len(temp_shortest_path_values) - int(num_of_operators)))
        temp_primal_LP_result = optimizeLP(temp_mastermatrix, rightHS, temp_column_names, temp_shortest_path_values, ["L"]*len(rightHS), "max", row_names, [0]*len(temp_shortest_path_values), temp_upperbound)
        temp_x = np.asarray(temp_primal_LP_result[1])
        temp_dual_LP_result = optimizeLP(np.asarray(temp_mastermatrix).transpose(), temp_shortest_path_values, row_names, rightHS, ["G"]*len(temp_shortest_path_values), "min", temp_column_names, [0]*len(rightHS), [cp.infinity]*len(rightHS))
        temp_y = np.asarray(temp_dual_LP_result[1])
        temp_primal_value = np.matmul(temp_primal_LP_result[1], temp_shortest_path_values)
        heuristic_estimation.append(temp_primal_value)
        temp_dual_value= np.matmul(temp_y, rightHS)
        temp_column_candidates =[]
        temp_heur_values=[]
        temp_reduced_cost=[]
        temp_sol_types=[]
        for i in range(0, num_of_abstractions):
            temp_output= get_and_transform_costpart_b(i, abstractionsLPs, num_task_operators, abstractions, temp_y, num_of_operators)
            temp_sol_types.append(temp_output[0])
            temp_column_candidates.append(temp_output[1])
            H= shortest_path(i, abstractions, temp_output[3], num_of_operators, operator_costs)[1]
            temp_heur_values.append(H)
            temp_reduced_cost.append(temp_y.dot(temp_output[1]) - H)
        #print temp_sol_types, "temp_sol_types"
        #print temp_column_candidates, "column candidates"
        #print temp_heur_values, "heur_values"
        #print temp_reduced_cost, "reduced_cost"
        if (num_of_iterations % (10**int(math.log(num_of_iterations, 10))) == 0):
            print "Iteration %d [%f s, current value: %f]" % (num_of_iterations, time.time() - start_loop_time, temp_primal_value)

    print "[finalHeuristicValue]", temp_primal_value
    print "[numberOfIterations]", num_of_iterations
    print "[heuristicEstimation]", heuristic_estimation
    return 1

def create_lps(abstractions, num_of_operators):
    """Takes the abstractions and creates the linear program for each of them.

    Args: the abstraction instances, number of operators

    Returns: abstractionsLPs vector, the i`th entry belongs to the i`th abstraction and is an array consisting of:
                matrix, rHS, columnNames, target, relationSenses, rowNames, lowerBound, upperBound, abstraction.num_abstract_states, operatorIDs
    """
    abstractionsLPs=[ [] for i in range(len(abstractions))]
    for var, abstraction in enumerate(abstractions):
        target = [0]* (abstraction.num_abstract_states + len(abstractions[var].relevant_operators))
        target[abstraction.goal_state] =1
        columnNames=["Abstract state "+`i` for i in range(abstraction.num_abstract_states)]
        rowNames=[]
        matrix =[]
        part_upperBound=[]
        part_lowerBound=[]
        relationSenses =[]
        rHS=[0]*len(abstractions[var].relevant_operators)
        operatorIDs=[]
        for item, transition in enumerate(abstractions[var].transitions):
            if transition[1] < num_of_operators:
                part_upperBound.append(cp.infinity)
                part_lowerBound.append(-cp.infinity)
            elif transition[1] >= num_of_operators:
                part_upperBound.append(0)
                part_lowerBound.append(0)
            operatorIDs.append(transition[1])
            columnNames.append("Operator "+`transition[1]`)
            rowNames.append("Operator "+`transition[1]`)
            row = np.zeros(abstraction.num_abstract_states + len(abstractions[var].relevant_operators))#baseVector
            if transition[0] != transition[2]:
                row[int(transition[0])] = -1.0
                row[int(transition[2])] = 1.0
                row[int(abstraction.num_abstract_states + item)] = -1.0
                relationSenses.append("L")
            elif transition[0] == transition[2]:
                row[int(abstraction.num_abstract_states + item)] = 1.0
                relationSenses.append("G")
            matrix.append(row)
        lowerBound=[0]*abstraction.num_abstract_states + part_lowerBound
        upperBound=[cp.infinity] * abstraction.num_abstract_states + part_upperBound
        upperBound[abstraction.initial_state]=0
        abstractionsLPs[var].append(matrix)
        abstractionsLPs[var].append(rHS)
        abstractionsLPs[var].append(columnNames)
        abstractionsLPs[var].append(target)
        abstractionsLPs[var].append(relationSenses)
        abstractionsLPs[var].append(rowNames)
        abstractionsLPs[var].append(lowerBound)
        abstractionsLPs[var].append(upperBound)
        abstractionsLPs[var].append(abstraction.num_abstract_states)
        abstractionsLPs[var].append(operatorIDs)
    return abstractionsLPs

def shortest_path(abstraction_index, abstractions, cost, num_of_operators, operator_costs):
    """A function for calculationg the shortest path in a given graphs

    Args: abstraction_index: number of the abstraction, abstractions, cost of the edges

    Returns:
    """
    matrix=[]
    transition_costs=[]
    for i in range(0, len(abstractions[abstraction_index].transitions)):
        if int(abstractions[abstraction_index].transitions[i][1]) < num_of_operators:
            transition_costs.append(operator_costs[int(abstractions[abstraction_index].transitions[i][1])])
        else:
            transition_costs.append(0)
        usedvec=[0]*abstractions[abstraction_index].num_abstract_states
        if abstractions[abstraction_index].transitions[i][0] != abstractions[abstraction_index].transitions[i][2]:
            usedvec[int(abstractions[abstraction_index].transitions[i][0])] = -1.0
            usedvec[int(abstractions[abstraction_index].transitions[i][2])] = 1.0
        matrix.append(usedvec)
    transposed_matrix= map(list, zip(*matrix))
    inst_rhs=[0]*len(transposed_matrix)
    inst_rhs[abstractions[abstraction_index].initial_state] = -1.0
    inst_rhs[abstractions[abstraction_index].goal_state] = 1.0
    inst_senses=["E"]*len(transposed_matrix)
    rownames=["Abstract state "+`i` for i in range(abstractions[abstraction_index].num_abstract_states)]
    colnames=["Operator "+`abstractions[abstraction_index].transitions[i][1]` for i in range(len(abstractions[abstraction_index].transitions))]
    lb = [0] *len(cost)
    ub = [1] *len(cost)
    answer1= optimizeLP(transposed_matrix, inst_rhs , colnames, cost, inst_senses, "min", rownames , lb, ub)
    costfunct = np.array(answer1[1], dtype= np.float64)
    transcosts= np.array(transition_costs, dtype= np.float64)
    answer2 = np.dot(costfunct, transcosts)
    answer3= np.dot(answer1[1], cost)
    answer= [answer1, answer3]
    return answer

def get_and_transform_costpart_a(i, abstractionsLPs, num_task_operators, abstractions):
    """For the i`th abstraction it gets an extreme point or ray

    Args: the index of obstraction, the abstraction LP`s and the number of operators in the original task

    Returns: unbounded or optimal, extreme ray or point
    """
    #print i, "abst.number"
    cost_vector=[0]* num_task_operators   #for each operator in the whole task a cost
    index=0
    a = optimizeLP(abstractionsLPs[i][0], abstractionsLPs[i][1], abstractionsLPs[i][2], (np.random.rand(len(abstractionsLPs[i][3])))*10-5, abstractionsLPs[i][4], "min", abstractionsLPs[i][5], abstractionsLPs[i][6], abstractionsLPs[i][7])
    goal= a[1][int(abstractions[i].goal_state)]
    operator_costfunction= a[1][- len(abstractions[i].transitions) :]
    solution_type =a[0]
    #print solution_type
    answer= []
    if solution_type == "unbounded":
        costs = a[1][- len(abstractionsLPs[i][9]):]
        indices = abstractionsLPs[i][9]
        while indices[index] < num_task_operators :
            cost_vector[indices[index]] = costs[index]
            index = index +1
            if index == len(indices):
                break
        answer.append("unbounded")
        answer.append(cost_vector)
        answer.append(goal)
        answer.append(operator_costfunction)
        answer.append(a)
    if solution_type == "optimal":
        costs = a[1][- len(abstractionsLPs[i][9]):]
        indices = abstractionsLPs[i][9]
        while indices[index] < num_task_operators :
            cost_vector[indices[index]] = costs[index]
            index = index +1
            if index == len(indices):
                break
        answer.append("optimal")
        answer.append(cost_vector) #the vector we will be using at the masterproblem
        answer.append(goal)
        answer.append(operator_costfunction) #the cost vector containing the costs given to the forget operators as well
    return answer


def get_and_transform_costpart_b(i, abstractionsLPs, num_task_operators, abstractions, yfunction, num_of_operators):
    """For the i`th abstraction it gets an extreme point or ray

    Args: yfunction comes from the master problem. it is part of the target function

    Returns: unbounded or optimal, extreme ray or point
    """
    #print i, "abst.number"
    rel_operators=sorted(abstractions[i].relevant_operators)
    cost_vector=[0]* num_task_operators
    function_cost_vector=[0]* abstractions[i].num_abstract_states
    function_cost_vector[int(abstractions[i].goal_state)]=-1  #for each operator in the whole task a cost
    for k in range(0, len(rel_operators)):
        if rel_operators[k] < len(yfunction):
            function_cost_vector.append(yfunction[int(rel_operators[k])])
        else:
            function_cost_vector.append(0)
    a = optimizeLP(abstractionsLPs[i][0], abstractionsLPs[i][1], abstractionsLPs[i][2], function_cost_vector, abstractionsLPs[i][4], "min", abstractionsLPs[i][5], abstractionsLPs[i][6], abstractionsLPs[i][7])
    goal= a[1][int(abstractions[i].goal_state)]
    operator_costfunction= a[1][- len(abstractions[i].transitions) :]
    solution_type =a[0]
    answer= []
    index=0
    if solution_type == "unbounded":
        costs = a[1][- len(abstractionsLPs[i][9]):]
        indices = abstractionsLPs[i][9]
        while indices[index] < num_task_operators :
            cost_vector[indices[index]] = costs[index]
            index = index +1
            if index == len(indices):
                break
        answer.append("unbounded")
        answer.append(cost_vector)
        answer.append(goal)
        answer.append(operator_costfunction)
        answer.append(a)
    if solution_type == "optimal":
        costs = a[1][- len(abstractionsLPs[i][9]):]
        indices = abstractionsLPs[i][9]
        while indices[index] < num_task_operators :
            cost_vector[indices[index]] = costs[index]
            index = index +1
            if index == len(indices):
                break
        answer.append("optimal")
        answer.append(cost_vector) #the vector we will be using at the masterproblem
        answer.append(goal)
        answer.append(operator_costfunction) #the cost vector containing the costs given to the forget operators as well
    return answer


def main(domain_filename, task_filename):
    start_time = time.time()
    print "Parsing task ..."
    task = pddl_parser.open(domain_filename=domain_filename, task_filename=task_filename)
    normalize.normalize(task)
    sas_task = pddl_to_sas(task)
    print "Done parsing task [%f s]" % (time.time() - start_time)

    num_of_operators= len(sas_task.operators)
    num_of_abstractions= len(sas_task.variables.value_names)

    #1. loading, normalizing and creating the sas task

    operator_costs=[]
    for op in range(0, len(sas_task.operators)):
        operator_costs.append(sas_task.operators[op].cost)
    num_task_operators =len(sas_task.operators)

    #2. creating the abstractions and the linear program for each abstraction
    print "creating abstractions ... "
    start_abstractions_time = time.time()
    abstractions = create_atomic_projections(sas_task)
    print "Done creating abstractions [%f s]" % (time.time() - start_abstractions_time)
    print "creating LPs ... "
    start_creating_lps_time = time.time()
    abstractionsLPs = create_lps(abstractions, num_of_operators)
    print "Done creating LPs [%f s]" % (time.time() - start_creating_lps_time)
    #print len(sas_task.operators)
    #3. The costpart_loop function creates the master problem: fills the matrix of the master lp with extreme rays of all subpolyhedrons
    print "Starting cost partitioning loop"
    iterations=  costpart_loop(num_of_operators, abstractionsLPs, num_task_operators, abstractions, operator_costs, num_of_abstractions, start_time)
    counter=0
    #while iterations < 0:
    #    counter= counter +1
    #    iterations=  costpart_loop(num_of_operators, abstractionsLPs, num_task_operators, abstractions, operator_costs, num_of_abstractions, start_time)
    print "[runningTime]", time.time() - start_time



if __name__ == "__main__":
    if len(sys.argv) != 3:
        print "Needs three arguments"
        sys.exit(1)
    main(sys.argv[1], sys.argv[2])
