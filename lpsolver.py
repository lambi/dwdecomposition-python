#!/usr/bin/env python

import sys
import os
dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(dir_path, "fast-downward-src"))
import pddl_parser
import normalize
import numpy as np
import cplex as cp
from translate import pddl_to_sas
from abstractions import create_atomic_projections
from numpy.linalg import inv
import time


def create_LP_ingredients(abstractions, num_of_operators):
    matrix_of_LP = []
    target_of_LP=[]
    lower_bound_of_LP=[]
    upper_bound_of_LP=[]
    row_names=[]
    column_names= []
    length_for_first_zeros=0
    relationSenses = []
    num_of_all_columns=0
    for var, abstraction in enumerate(abstractions):
        num_of_all_columns = num_of_all_columns + abstraction.num_abstract_states + len(abstractions[var].relevant_operators)

    costpart_rows=[]
    for i in range(0, num_of_operators):
        costpart_rows.append(np.zeros(num_of_all_columns))

    for var, abstraction in enumerate(abstractions):
        first_zeros = np.zeros(length_for_first_zeros)
        row = np.zeros(abstraction.num_abstract_states + len(abstractions[var].relevant_operators))#baseVector
        num_of_last_zeros= num_of_all_columns - length_for_first_zeros - len(row)
        target = [0]* (abstraction.num_abstract_states + len(abstractions[var].relevant_operators))
        target[abstraction.goal_state] =1
        subpart_upperBound=[]
        subpart_lowerBound=[]
        for item, transition in enumerate(abstractions[var].transitions):
            if transition[1] < num_of_operators:
                subpart_upperBound.append(cp.infinity)
                subpart_lowerBound.append(-cp.infinity)
            elif transition[1] >= num_of_operators:
                subpart_upperBound.append(0)
                subpart_lowerBound.append(0)
            row = np.zeros(abstraction.num_abstract_states + len(abstractions[var].relevant_operators))#baseVector
            if transition[0] != transition[2]:
                row[int(transition[0])] = -1.0
                row[int(transition[2])] = 1.0
                row[int(abstraction.num_abstract_states + item)] = -1.0
                relationSenses.append("L")
                if transition[1] < num_of_operators:
                    costpart_rows[int(transition[1])][int(length_for_first_zeros + abstraction.num_abstract_states + item)] =1
            elif transition[0] == transition[2]:
                row[int(abstraction.num_abstract_states + item)] = 1.0
                relationSenses.append("G")
                if transition[1] < num_of_operators:
                    costpart_rows[int(transition[1])][int(length_for_first_zeros + item)] =1
            last_zeros=np.zeros(num_of_last_zeros)
            row_to_append= np.concatenate((first_zeros, row, last_zeros), axis=None)
            matrix_of_LP.append(row_to_append)
        length_for_first_zeros = length_for_first_zeros + abstraction.num_abstract_states + len(abstractions[var].relevant_operators)
        target_of_LP = np.concatenate((target_of_LP, target), axis=None)
        part_lowerBound=[0]*abstraction.num_abstract_states + subpart_lowerBound
        part_upperBound=[cp.infinity] * abstraction.num_abstract_states + subpart_upperBound
        part_upperBound[abstraction.initial_state]=0
        lower_bound_of_LP=lower_bound_of_LP + part_lowerBound
        upper_bound_of_LP=upper_bound_of_LP + part_upperBound
    for i in range(0, num_of_operators):
        matrix_of_LP.append(costpart_rows[i])
        relationSenses.append("L")
    for i in range(0, len(relationSenses)):
        row_names.append("row "+`i`)
    for i in range(0, len(lower_bound_of_LP)):
        column_names.append("column "+`i`)
    lpData=[]
    lpData.append(matrix_of_LP)
    lpData.append(target_of_LP)
    lpData.append(lower_bound_of_LP)
    lpData.append(upper_bound_of_LP)
    lpData.append(row_names)
    lpData.append(column_names)
    lpData.append(relationSenses)
    return lpData

def optimizeLP(A, Rhs, Names, Objective, Senses, optSense, conNames, lower_bounds, upper_bounds):
    """Call the LP solver of Cplex, optimizing
    Args:
        A (array): array of arrays (originally array of row-vectors)
        Rhs (array): right hand side
        Names (array): column names
        Objective (array): objective function
        Senses (array): array containing relational signs for every row, can be one of equal(E), lessthanequal(L), greaterthanequal(G)
        optSense (string): "min" or "max" accordint to how we want to optimize
        conNames (array): constraint names
    Returns:
        if the LP has optimal solition: tuple("optimal", optimal solution)
        if the LP is unbounded: tuple("unbounded", extreme ray)
        if the LP is infeasible: string "infeasible"
    """
    problem = cp.Cplex()
    problem.set_log_stream(None)
    #problem.set_error_stream(None)
    problem.set_warning_stream(None)
    problem.set_results_stream(None)
    if optSense == "max":
        problem.objective.set_sense(problem.objective.sense.maximize)
    else:
        problem.objective.set_sense(problem.objective.sense.minimize)
    names = Names
    problem.variables.add(obj = Objective, lb = lower_bounds, ub = upper_bounds, names = Names)
    constraints = map(lambda row: [names, row], A)
    problem.linear_constraints.add(lin_expr = constraints,
                                   senses = Senses,
                                   rhs = Rhs)
    fake_vector=[0.0]*len(names)
    problem.parameters.preprocessing.presolve.set(problem.parameters.preprocessing.presolve.values.off)
    problem.solve()
    status = problem.solution.get_status_string()
    if status == "unbounded" :
        ray = problem.solution.advanced.get_ray()
    else: ray = "-1"
    value= problem.solution.get_values()
    number= problem.solution.get_objective_value()
    if status == "optimal":
        return (status, value, number)
    elif status == "unbounded":
        return (status, ray, "unbounded")
    else: return (status, fake_vector)


def main(domain_filename, task_filename):
    start_time = time.time()
    task = pddl_parser.open(domain_filename=domain_filename, task_filename=task_filename)
    normalize.normalize(task)
    sas_task = pddl_to_sas(task)

    num_of_operators= len(sas_task.operators)
    num_of_abstractions= len(sas_task.variables.value_names)
    #1. loading, normalizing and creating the sas task

    operator_costs=[]
    for op in range(0, len(sas_task.operators)):
        #print sas_task.operators[op].name
        operator_costs.append(sas_task.operators[op].cost)
    num_task_operators =len(sas_task.operators)

    abstractions = create_atomic_projections(sas_task)

    data_for_cplex= create_LP_ingredients(abstractions, num_of_operators)
    num_of_rows= len(data_for_cplex[0])
    rhs = [0]* (num_of_rows - len(operator_costs) ) + operator_costs

    result= optimizeLP(data_for_cplex[0], rhs, data_for_cplex[5], data_for_cplex[1], data_for_cplex[6], "max", data_for_cplex[4], data_for_cplex[2], data_for_cplex[3])
    value= result[2]
    print "[finalHeuristicValue]", value
    print "[runningTime]", time.time() - start_time




if __name__ == "__main__":
    if len(sys.argv) != 3:
        print "Needs three arguments"
        sys.exit(1)
    main(sys.argv[1], sys.argv[2])
