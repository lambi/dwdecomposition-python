#!/usr/bin/env python

import re
from properties import Properties

DONE_PARSING_LINE = re.compile("^Done parsing task \[(?P<time>.+) s\]")
DONE_ABSTRACTIONS_LINE = re.compile("^Done creating abstractions \[(?P<time>.+) s\]")
DONE_CREATING_LPS_LINE = re.compile("^Done creating LPs \[(?P<time>.+) s\]")
ITERATION_LINE = re.compile("^Iteration (?P<iteration>\d+) \[(?P<time>.+) s, current value: (?P<value>.+)\]")
FINAL_VALUE_LINE = re.compile("^\[finalHeuristicValue\] (?P<value>.+)")
NUM_ITERATIONS_LINE = re.compile("^\[numberOfIterations\] (?P<value>\d+)")
HEURISTIC_ESTIMATIONS_LINE = re.compile("^\[heuristicEstimation\] \[(?P<list>.+)\]")
RUNNING_TIME_LINE = re.compile("^\[runningTime\] (?P<time>.+)")
OUT_OF_MEMORY_ERROR = re.compile("^CPLEX Error  1001: Out of memory.")
TIMEOUT_ERROR = re.compile("^ValueError: Timeout.")

def parse_float(line, regex, regex_name, prop_name, props):
    match = re.match(regex, line)
    if match:
        props[prop_name] = float(match.group(regex_name))

def parse_int(line, regex, regex_name, prop_name, props):
    match = re.match(regex, line)
    if match:
        props[prop_name] = int(match.group(regex_name))

def parse_log_properties(content, props):
    for line in content.splitlines():
        parse_float(line, DONE_PARSING_LINE, "time", "parsing_time", props)
        parse_float(line, DONE_ABSTRACTIONS_LINE, "time", "creating_abstractions_time", props)
        parse_float(line, DONE_CREATING_LPS_LINE, "time", "creating_lps_time", props)
        parse_float(line, FINAL_VALUE_LINE, "value", "final_value", props)
        parse_float(line, DONE_PARSING_LINE, "time", "parsing_time", props)
        parse_float(line, RUNNING_TIME_LINE, "time", "total_time", props)
        parse_int(line, NUM_ITERATIONS_LINE, "value", "iterations", props)

        match = re.match(ITERATION_LINE, line)
        if match:
            iteration = match.group("iteration")
            time = float(match.group("time"))
            value = float(match.group("value"))
            props["iteration_%s_time" % iteration] = time
            props["iteration_%s_value" % iteration] = value
        match = re.match(HEURISTIC_ESTIMATIONS_LINE, line)
        if match:
            values = [float(e.strip()) for e in match.group("list").split(",")]
            props["heuristic_estimations"] = values

def parse_err_properties(content, props):
    err_empty = True
    for line in content.splitlines():
        err_empty = False
        match = re.match(OUT_OF_MEMORY_ERROR, line)
        if match:
            props["out_of_memory_error"] = True
        match = re.match(TIMEOUT_ERROR, line)
        if match:
            props["timeout_error"] = True
    props["run_err_empty"] = err_empty


def parse_files(log_file, err_file):
    props = Properties('info')
    with open(log_file) as f:
        log_content = f.read()
        parse_log_properties(log_content, props)
    with open(err_file) as f:
        err_content = f.read()
        parse_err_properties(err_content, props)
    props.write()


parse_files("run.log", "run.err")
